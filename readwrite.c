#include <stdio.h>
#include "lmdb.h"

#define print_error_and_return(func, code) { printf("Error %s: %d %s\n", func, code, mdb_strerror(code)); return -1; }

int main()
{
	MDB_env * env;
	MDB_txn * txn;
	MDB_dbi dbi;

	int ret = 0;

	printf("starting up...\n");

	ret = mdb_env_create(&env);
	if(ret) {
		print_error_and_return("mdb_env_create", ret);
	}
	printf("mdb_env_create...ok\n");

	ret = mdb_env_open(env, "./testdb", MDB_FIXEDMAP, 0664);
	if(ret) {
		print_error_and_return("mdb_env_open", ret);
	}
	printf("mdb_env_open...ok\n");
	
	ret = mdb_txn_begin(env, NULL, MDB_RDONLY, &txn);
	if(ret) {
		print_error_and_return("mdb_txn_begin", ret);
	}
	printf("mdb_txn_begin...ok\n");

	ret = mdb_dbi_open(txn, NULL, 0, &dbi);
	if(ret) {
		print_error_and_return("mdb_open", ret);
	}
	printf("mdb_open...ok\n");

	mdb_close(env, dbi);
	mdb_txn_abort(txn);
	mdb_env_close(env);
	printf("done\n");

	return 0;
}
