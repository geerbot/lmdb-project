# README #

### What is this repository for? ###

Getting comfortable with LMDB


### How do I get set up? ###

(verify testdb directory exists)
make
LD_LIBRARY_PATH=lmdb/libraries/liblmdb
./readwrite


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
