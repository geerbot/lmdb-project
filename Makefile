CC = gcc

INCDIR=-Ilmdb/libraries/liblmdb
LIBDIR=-Llmdb/libraries/liblmdb
LIBS=-llmdb
PROGS = readwrite

all: $(PROGS)

readwrite:  
	$(CC) $(CFLAGS) $(INCDIR) -o readwrite readwrite.c $(LIBDIR) $(LIBS)

clean:
	rm readwrite
